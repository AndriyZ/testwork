//
//  UserData.swift
//  TestWork
//
//  Created by ZAV on 10.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

/** Class include all user data */
class UserData {
    
    /** ID user */
    var id: Int = 0
    
    /** First name user */
    var firstName: String = ""
    
    /** Last name user */
    var lastName: String = ""
    
    /** Return full name */
    var fullName: String {
        get {
            return "\(self.firstName) \(self.lastName)"
        }
    }
    
    /** Email user */
    var email: String = ""
    
    /** Address image user */
    var imageAddress: String = ""
    
    /** Latitude coordinate */
    var lat: Double = 0.0
    
    /** Longitude coordinate */
    var lon: Double = 0.0
    
    /** Created date user (unix time) */
    var createdAt: Int = 0
    
    /** Updated date user (unix time) */
    var updatedAt: Int = 0
    
    /** City user */
    var city: String = ""
    
    /** Country user */
    var country: String = ""
    
    /** Return place */
    var place: String {
        get {
            var place = ""
            if self.city.characters.count > 0 && self.city != "null" {
                place = self.city
                if self.country.characters.count > 0 && self.country != "null"  {
                    place += ", \(self.country)"
                }
            }
            return place
        }
    }
    
    /** Gender user */
    var gender: String = ""
    
    /** 
        Return int from gender
        male - 1
        female - 2
    */
    var typeGender: Int {
        set {
            if newValue == 0 {
                self.gender = "male"
            } else {
                self.gender = "female"
            }
        }
        get {
            return gender == "male" ? 0 : 1
        }
    }
    
    /** Init from JSON */
    init(data: JSON) {
        
        if let id = data["id"].int {
            self.id = id
        }
        if let email = data["email"].string {
            self.email = email
        }
        if let createdAt = data["created_at"].int {
            self.createdAt = createdAt
        }
        if let updatedAt = data["updated_at"].int {
            self.updatedAt = updatedAt
        }
        if let image = data["image"].string {
            self.imageAddress = image
        }
        if let lat = data["lat"].string {
            self.lat = Double(lat)!
        }
        if let lon = data["lon"].string {
            self.lon = Double(lon)!
        }
        if let firstName = data["first_name"].string {
            self.firstName = firstName
        }
        if let lastName = data["last_name"].string {
            self.lastName = lastName
        }
        if let city = data["city"].string {
            self.city = city
        }
        if let country = data["country"].string {
            self.country = country
        }
        if let gender = data["gender"].string {
            self.gender = gender
        }

    }
    
    /** Standart init */
    init() {}
}
