//
//  Server.swift
//  TestWork
//
//  Created by ZAV on 10.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//


/** Address server */
let address = "http://test-api.live.gbksoft.net/api/v1/"


/** Class include all request to server */
class Server {
  
    /** 
        Method login
        Return
        - status: Bool
    */
    static func login(result: (status: Bool) -> Void) -> Void {
        
        let parameters = ["fb_token" : FBSDKAccessToken.currentAccessToken().tokenString]
        
        // send request login
        request(.POST, address + "login", parameters: parameters)
            .responseJSON { response in
                
                // get data
                if let valueJSON = response.result.value where response.result.isSuccess {
                    
                    let dataJSON = JSON(valueJSON)
                    
                    // get status
                    if let token = dataJSON["message"].string where dataJSON["code"].int == 200 {
                        SyncData.saveData(token, key: KeySyncData.TokenServer)
                        result(status: true)
                    } else {
                        result(status: false)
                    }
                    
                } else {
                    result(status: false)
                }
        }
        
    }
    
    /**
        Method location
        Return
        - status: Bool
    */
    static func location(result: (status: Bool) -> Void) -> Void {
        
        let parameters = ["lat" : SyncData.getDoubleData(KeySyncData.Lat), "lon" : SyncData.getDoubleData(KeySyncData.Lon)]
        
        // send request location
        request(.POST, address + "location?token=\(SyncData.getStringData(KeySyncData.TokenServer))", parameters: parameters)
            .responseJSON { response in
                
                // get data
                if let valueJSON = response.result.value where response.result.isSuccess {
                    
                    //print("save coordinate")
                    
                    SyncData.saveIntData(Int(NSDate().timeIntervalSince1970), key: KeySyncData.UpdatedAt)
                    
                    // get status
                    if JSON(valueJSON)["code"].int == 200 {
                        result(status: true)
                    } else {
                        result(status: false)
                    }
                    
                } else {
                    result(status: false)
                }
        }
        
    }
    
    /**
        Method Get Profile
        Return 
        - status: Bool
        - userData: UserData
    */
    static func getProfile(result: (status: Bool, userData: UserData) -> Void) -> Void {
        
        let parameter = "?token=\(SyncData.getStringData(KeySyncData.TokenServer))"
        
        // send request get profile
        request(.GET, address + "profile\(parameter)", parameters: nil)
            .responseJSON { response in
                
                // get data
                if let valueJSON = response.result.value where response.result.isSuccess {
                    
                    let dataJSON = JSON(valueJSON)
                    
                    // get status
                    if let data = dataJSON["result"].dictionary where dataJSON["code"].int == 200 {
                        result(status: true, userData: UserData(data: JSON(data)))
                    } else {
                        result(status: false, userData: UserData())
                    }
                    
                } else {
                    
                    result(status: false, userData: UserData())
                    
                }
                
        }
        
    }
    
    /**
        Method Set new value in profile
        Return
        - status: Bool
    */
    static func setProfile(image: UIImage, isNewImage: Bool = false , userData: UserData, result: (status: Bool) -> Void) -> Void {
        
        var parameters = [String : AnyObject]()
        
        if userData.firstName.characters.count > 0 {
            parameters["first_name"] = "\(userData.firstName)"
        }
        if userData.lastName.characters.count > 0 {
            parameters["last_name"] = "\(userData.lastName)"
        }
        if userData.gender.characters.count > 0 {
            parameters["gender"] = "\(userData.gender)"
        }
        if userData.city.characters.count > 0 {
            parameters["city"] = "\(userData.city)"
        }
        if userData.country.characters.count > 0 {
            parameters["country"] = "\(userData.country)"
        }

        if isNewImage {
            parameters["image"] = NetData(pngImage: image, filename: "image.png")
        }
        
        let url = address + "profile?token=\(SyncData.getStringData(KeySyncData.TokenServer))"
        
        let net = Net()
        
        // send request set profile
        net.POST(url, params: parameters, successHandler: { responseData in
            
            // get status
            let resultJSON = responseData.json(nil)
            NSLog("resultJSON: \(resultJSON)")
            result(status: true)
            
            }, failureHandler: { error in
                NSLog("Error")
                result(status: false)
                
        })
    }

    /**
        Method Array users
        Return
        - status: Bool
        - usersArray: [UserData]
    */
    static func arrayUsers(page: Int, perPage: Int = 10, result: (status: Bool, usersArray: [UserData]) -> Void) -> Void {
        
        var usersArray = [UserData]()

        let parameters = ["token" : SyncData.getStringData(KeySyncData.TokenServer), "page" : "\(page)", "per-page" : "\(perPage)"]
        
        
        // send request array users
        request(.GET, address + "users", parameters: parameters)
            .responseJSON { response in
                
                // get data
                if let valueJSON = response.result.value where response.result.isSuccess {
                    
                    let dataJSON = JSON(valueJSON)
                    
                    // get status
                    if let array = dataJSON["result"].array where dataJSON["code"].int == 200 {
                        for data in array {
                            let userData = UserData(data: data)
                            if userData.id != 0 {
                                usersArray.append(userData)
                            }
                        }
                        result(status: true, usersArray: usersArray)
                    } else {
                        result(status: false, usersArray: usersArray)
                    }
                
                } else {
                    
                    result(status: false, usersArray: usersArray)
                    
                }
                
        }
    }
    
    
    
    /**
        Method Array users search
        Return
        - status: Bool
        - usersArray: [UserData]
    */
    static func arrayUsersSearch(searchString: String = "", radius: Int = 0, lat: Double = 0.0, lon: Double = 0.0, result: (status: Bool, usersArray: [UserData]) -> Void) -> Void {
        
        var usersArray = [UserData]()
        
        var parameters = ["token" : SyncData.getStringData(KeySyncData.TokenServer)]
        
        if searchString.characters.count > 0 {
            parameters["search_string"] = searchString
        }
        if radius > 0 {
            parameters["radius"] = "\(radius)"
        }
        if lat > 0.0 {
            parameters["lat"] = "\(lat)"
        }
        if lon > 0.0 {
            parameters["lon"] = "\(lon)"
        }
        
        // send request array users search
        request(.GET, address + "users/search", parameters: parameters)
            .responseJSON { response in
                
                // get data
                if let valueJSON = response.result.value where response.result.isSuccess {
                    
                    let dataJSON = JSON(valueJSON)
                    
                    // get status
                    if let array = dataJSON["result"].array where dataJSON["code"].int == 200 {
                        for data in array {
                            let userData = UserData(data: data)
                            if userData.id != 0 {
                                usersArray.append(userData)
                            }
                        }
                        result(status: true, usersArray: usersArray)
                    } else {
                        result(status: false, usersArray: usersArray)
                    }
                    
                } else {
                    
                    result(status: false, usersArray: usersArray)
                    
                }
        }
    }
}
