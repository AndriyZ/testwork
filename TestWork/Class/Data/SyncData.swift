//
//  SyncData.swift
//  TestWork
//
//  Created by ZAV on 11.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit


/** Enum key for NSUserDefaults */
enum KeySyncData: String {
    case TokenServer = "TokenServer"    // Token server api
    case Lat = "lat"                    // lat last update to server
    case Lon = "lon"                    // lon last update to server
    case UpdatedAt = "updatedAt"        // time unix last update to server
}

/** Class for data synchronization */
class SyncData {
    
    /** Save data (AnyObject) */
    static func saveData(data: AnyObject, key: KeySyncData) {

        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        userDefaults.setObject(data, forKey: key.rawValue)
        
        userDefaults.synchronize()
    }
    
    /** Get data (String) */
    static func getStringData(key: KeySyncData) -> String {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        if let text: AnyObject = userDefaults.objectForKey(key.rawValue) {
            return text as! String
        }
        
        return ""
    }
    
    /** Save data (Int) */
    static func saveIntData(data: Int, key: KeySyncData) {
        
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        userDefaults.setInteger(data, forKey: key.rawValue)
        
        userDefaults.synchronize()
    }
    
    /** Get data (Int) */
    static func getIntData(key: KeySyncData) -> Int {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        return userDefaults.integerForKey(key.rawValue)
    }
    
    /** Save data (Double) */
    static func saveDoubleData(data: Double, key: KeySyncData) {
        
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        userDefaults.setDouble(data, forKey: key.rawValue)
        
        userDefaults.synchronize()
    }
    
    /** Get data (Double) */
    static func getDoubleData(key: KeySyncData) -> Double {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        return userDefaults.doubleForKey(key.rawValue)
        
    }

}
