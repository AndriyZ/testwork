//
//  MarkerData.swift
//  TestWork
//
//  Created by ZAV on 12.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit

/** Class include all marker data */
class MarkerData {

    /** Array all users */
    var arrayUser: [UserData] = [UserData]()
    
    /** Latitude coordinate */
    var lat: Double = 0.0
    
    /** Longitude coordinate */
    var lon: Double = 0.0
    
    /** Flag is remove (true - remove) */
    var isFlag: Bool = false
    
    /** Init new user data */
    init(newUserData: UserData) {
        self.addNewUser(newUserData)
    }
    
    /** Add new user data */
    func addNewUser(newUserData: UserData) {
        arrayUser.append(newUserData)
        
        if arrayUser.count <= 1 {
            lat = newUserData.lat
            lon = newUserData.lon
        } else {
            self.middlePointOfListMarkers()
        }
    }
    
    
    /** Add new marker data */
    func addNewMarker(newMarker: MarkerData) {
        
        for data in newMarker.arrayUser {
            self.addNewUser(data)
        }
    }
    
    /** Return to lat and lon middle point */
    func middlePointOfListMarkers() {
        
        var x = 0.0 as CGFloat
        var y = 0.0 as CGFloat
        var z = 0.0 as CGFloat
        
        for user in self.arrayUser {
    
            let coordinate = CLLocationCoordinate2DMake(user.lat, user.lon)
            
            let lat:CGFloat = degreeToRadian(coordinate.latitude)
            let lon:CGFloat = degreeToRadian(coordinate.longitude)
            
            x = x + cos(lat) * cos(lon)
            y = y + cos(lat) * sin(lon);
            z = z + sin(lat);
            
        }
        
        x = x/CGFloat(self.arrayUser.count)
        y = y/CGFloat(self.arrayUser.count)
        z = z/CGFloat(self.arrayUser.count)
        
        let resultLon: CGFloat = atan2(y, x)
        let resultHyp: CGFloat = sqrt(x*x+y*y)
        let resultLat:CGFloat = atan2(z, resultHyp)
        
        self.lat  = radianToDegree(resultLat)
        self.lon  = radianToDegree(resultLon)
    }
    
    /** Degrees to Radian **/
    func degreeToRadian(angle:CLLocationDegrees) -> CGFloat{
        return (  (CGFloat(angle)) / 180.0 * CGFloat(M_PI)  )
    }
    
    /** Radians to Degrees **/
    func radianToDegree(radian:CGFloat) -> CLLocationDegrees{
        return CLLocationDegrees(  radian * CGFloat(180.0 / M_PI)  )
    }
    
    /** Standart init */
    init() {}
}
