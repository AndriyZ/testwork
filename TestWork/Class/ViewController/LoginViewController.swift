//
//  LoginViewController.swift
//  TestWork
//
//  Created by ZAV on 09.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit

/** Class UI Login */
class LoginViewController: UIViewController {

    /** Label for show loading */
    @IBOutlet weak var loadingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loginFB()
    }
    
    /** Create button login facebook */
    func loginFB() {
        if (FBSDKAccessToken.currentAccessToken() != nil) {
            // User is already logged in, do work such as go to next view controller.
            self.loginInServer()
        }
        else {
            // Hidden label for loading
            self.loadingLabel.hidden = true
            
            // User login
            let loginView : FBSDKLoginButton = FBSDKLoginButton()
            self.view.addSubview(loginView)
            loginView.center = self.view.center
            loginView.readPermissions = ["email", "public_profile"]
            loginView.delegate = self
        }
        
    }
    
    /** Login in server */
    func loginInServer() {
        
        Server.login({ (status) -> Void in
            if status {
                self.performSegueWithIdentifier("toTabBarFromLogin", sender: self)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - Facebook Delegate Methods

extension LoginViewController : FBSDKLoginButtonDelegate {
    
    /** After complete with result Login */
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("User Logged In")
        
        if ((error) != nil){
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            self.loginInServer()
        }
    }
    
    /** After complete with result Logout*/
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
}