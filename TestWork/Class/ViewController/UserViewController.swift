//
//  UserViewController.swift
//  TestWork
//
//  Created by ZAV on 11.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit

/** Class UI User */
class UserViewController: UIViewController {

    /** Image user */
    @IBOutlet weak var userImage: UIImageView!
    
    /** User name */
    @IBOutlet weak var name: UILabel!
    
    /** Sex user */
    @IBOutlet weak var sex: UILabel!
    
    /** Place user */
    @IBOutlet weak var place: UILabel!
    
    /** All user data */
    var userData: UserData = UserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadData()
    }
    
    /** Load data form UserData to UI */
    func loadData() {
        
        self.name.text = self.userData.fullName
        self.sex.text = self.userData.gender
        self.place.text = self.userData.place
        
        // request load image
        request(.GET, self.userData.imageAddress)
            .responseImage { response in
                
                if let image = response.result.value {
                    self.userImage.image = image
                }
        }
        
    }

    
    /** For load user data from other View Controller */
    func setUserData(newUserData: UserData) {
        self.userData = newUserData
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
