//
//  ListUsersViewController.swift
//  TestWork
//
//  Created by ZAV on 09.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit

/** Class UI List */
class ListUsersViewController: UIViewController {

    /** Search bar */
    @IBOutlet weak var searchBar: UISearchBar!
    
    /** Table view */
    @IBOutlet weak var tableView: UITableView!
    
    /** Array all users */
    var usersArray: [UserData] = [UserData]()
    
    /** Index select user */
    var indexSelectedUser: Int?
    
    /** Flag is loading data */
    var isLoading: Bool = false
    
    /** Index load page */
    var page: Int = 1
    
    /** Flag is all user loaded */
    var isFinishLoad: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set delegate and init all obj
        self.searchBar.delegate = self
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.loadData()
    }
    
    /** Method for load data from server */
    func loadData(nextPage nextPage: Bool = false) {
        
        if !self.isLoading && !self.isFinishLoad {
            
            self.isLoading = true
            
            if self.searchBar.text?.characters.count == 0 {
                
                // set next page
                if nextPage {
                    self.page += 1
                }
                
                self.isLoading = true
                
                // request array users
                Server.arrayUsers(self.page) { (status, usersArray) -> Void in
                    if status {
                        if self.page == 1 {
                            self.usersArray = usersArray
                        } else {
                            if self.usersArray.last?.id != usersArray.last?.id {
                                self.usersArray += usersArray
                            } else {
                                self.page -= 1
                                self.isFinishLoad = true
                                
                            }
                        }
                        
                        self.tableView.reloadData()
                        self.isLoading = false
                    }
                }
                
            } else {
                // request array users search
                Server.arrayUsersSearch(self.searchBar.text!, result: { (status, usersArray) -> Void in
                    if status {
                        self.usersArray = usersArray
                        self.tableView.reloadData()
                        self.isLoading = false
                    }
                    
                })

            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get segue to User View Controller
        if segue.identifier == "toUserFromList" {
            // Init user data in User View Controller
            let viewController:UserViewController = segue.destinationViewController as! UserViewController
            viewController.setUserData(self.usersArray[self.indexSelectedUser!])
        }
    }

}

// MARK: - UITableViewDataSource

extension ListUsersViewController: UITableViewDataSource {
    
    /** Get number table cell */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersArray.count
    }
    
    /** Get Table cell */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserTableViewCell", forIndexPath: indexPath) as! UserTableViewCell
        
        // get user data form array
        let userData = self.usersArray[indexPath.row]
        
        cell.nameUser.text = userData.fullName
        cell.placeUser.text = userData.place
        
        request(.GET, userData.imageAddress)
            .responseImage { response in
                
                if let image = response.result.value {
                    cell.imageUser.image = image
                }
        }
        
        // if last element load next page
        if indexPath.row >= self.usersArray.count - 2 {
            self.loadData(nextPage: true)
        }
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension ListUsersViewController: UITableViewDelegate {
   
    /** Select Table cell */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // load user view controller
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.indexSelectedUser = indexPath.row
        
        self.performSegueWithIdentifier("toUserFromList", sender: self)
    }
    
}



// MARK: - UISearchBarDelegate

extension ListUsersViewController: UISearchBarDelegate {
    
    /** Search bar edit begin */
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    /** Search bar cancel clicked */
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.page = 1
        self.loadData()
    }
    
    /** Search bar search clicked */
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.page = 1
        self.loadData()
    }
    
}
