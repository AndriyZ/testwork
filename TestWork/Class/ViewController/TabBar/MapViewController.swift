//
//  MapViewController.swift
//  TestWork
//
//  Created by ZAV on 09.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit
import CoreLocation

/** Class UI Map */
class MapViewController: UIViewController {

    /** Manager to show of the user on the map */
    let locationManager = CLLocationManager()
    
    /** Google map view */
    @IBOutlet weak var mapView: GMSMapView!
    
    /** Search Bar */
    @IBOutlet weak var searchBar: UISearchBar!
    
    /** Array all users */
    var markersArray: [MarkerData] = [MarkerData]()
    
    /** User data for profile view */
    var userData: UserData = UserData()
    
    /** Radius map from center */
    var radius: Int = 0
    
    /** Flag is show user on map */
    var isShowedUser: Bool = false
    
    /** Flag show is loaded marker on map */
    var isLoadedMarker: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set delegate and init all obj
        self.searchBar.delegate = self
        
        self.mapView.delegate = self
        self.locationManager.delegate = self
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapView.myLocationEnabled = true
        
    }
    
    /** Load data from server */
    func loadData() {
        
        // get point center and up
        let point = CGPointMake(self.mapView.frame.size.width / 2.0, self.mapView.frame.origin.y)
        
        // get radius
        self.radius = self.distance(CLLocationCoordinate2DMake(self.mapView.projection.coordinateForPoint(point).latitude, self.mapView.projection.coordinateForPoint(point).longitude), coorB: CLLocationCoordinate2DMake(self.mapView.camera.target.latitude, self.mapView.camera.target.longitude))
        
        // request array users search
        Server.arrayUsersSearch(self.searchBar.text!, radius: radius, lat: Double(self.mapView.camera.target.latitude), lon: Double(self.mapView.camera.target.longitude)) { (status, usersArray) -> Void in
            if status {
                self.markersArray = self.getMarkersDataFromUsersData(usersArray)
                self.filterMarkers()
                self.showData(radius: self.radius)
            }
        }

        
    }
    
    /** Show all marker on map */
    func showData(radius radius: Int) {
        
        // clear all old markers
        self.mapView.clear()
        self.mapView.myLocationEnabled = true
        
        // add marker to map
        for data in self.markersArray {
            let marker = GMSMarker(position: CLLocationCoordinate2DMake(data.lat, data.lon))
            marker.userData = data
            self.getImageFromMarkerData(data, result: { (image) -> Void in
                marker.icon = image
            })
            
            marker.map = mapView
        }
    }
    
    /** Set loaded marker true and load data form server */
    func changeLoadedMarker() {
        self.isLoadedMarker = true
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get segue to User View Controller
        if segue.identifier == "toUserFromMap" {
            // Init user data in User View Controller
            let viewController:UserViewController = segue.destinationViewController as! UserViewController
            viewController.setUserData(self.userData)
        }
    }
}

// MARK: - GMSMapViewDelegate

extension MapViewController: GMSMapViewDelegate {
    
    /** Tap on marker */
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        
        let markerData = marker.userData as! MarkerData
        
        // load user view controller if one marker
        if markerData.arrayUser.count == 1 {
            self.userData = markerData.arrayUser.last!
            
            self.performSegueWithIdentifier("toUserFromMap", sender: self)
        }
        
        return false
    }
    
    /** Changed camera position */
    func mapView(mapView: GMSMapView!, didChangeCameraPosition position: GMSCameraPosition!) {
        
        if self.isLoadedMarker {
            // load new marker by 1 second
            self.isLoadedMarker = false
            _ = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("changeLoadedMarker"), userInfo: nil, repeats: false)
        }
    }
}


// MARK: - CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    
    /** Upload coordinate */
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            
            // if location first coordinat
            if !self.isShowedUser {
                
                // Set camera
                self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
                
                self.isShowedUser = true
            }
            
            // Send user coordinate
            let lat = SyncData.getDoubleData(KeySyncData.Lat)
            let lon = SyncData.getDoubleData(KeySyncData.Lon)
            
            if SyncData.getIntData(KeySyncData.UpdatedAt) + (60 * 60) <= Int(NSDate().timeIntervalSince1970) || self.distance(CLLocationCoordinate2DMake(lat, lon), coorB: location.coordinate) > 1000 {
                
                SyncData.saveDoubleData(location.coordinate.latitude, key: KeySyncData.Lat)
                SyncData.saveDoubleData(location.coordinate.longitude, key: KeySyncData.Lon)
                
                // send new coordinat to server
                Server.location({ (status) -> Void in
                    
                })
            }
        }
        
    }
}


// MARK: - UISearchBarDelegate

extension MapViewController: UISearchBarDelegate {
    
    /** Search bar edit begin */
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    /** Search bar cancel clicked */
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.loadData()
    }
    
    /** Search bar search clicked */
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.loadData()
    }

}


// MARK: - Filter marker

extension MapViewController {
    
    /** Return array of marker user */
    func getMarkersDataFromUsersData(usersData: [UserData]) -> [MarkerData] {
        
        var arrayMarkers = [MarkerData]()
        
        // add all user data to new marker data
        for data in usersData {
            arrayMarkers.append(MarkerData(newUserData: data))
        }
        
        return arrayMarkers
    }
    
    
    /** Filter markers */
    func filterMarkers() {
        
        if self.markersArray.count > 0 {
            
            for indexBase in 0...self.markersArray.count - 1 {
                
                let markerBase = self.markersArray[indexBase]
                
                search: for index in indexBase...self.markersArray.count - 1 {
                    
                    let marker = self.markersArray[index]
                    
                    if indexBase != index {
                    
                        // get radius
                        let radius = self.distance(CLLocationCoordinate2DMake(marker.lat, marker.lon), coorB: CLLocationCoordinate2DMake(markerBase.lat, markerBase.lon))
                        
                        // calculation min size between two marker
                        let minSize = self.radius / 10
                        
                        // if marker intersect
                        if radius < minSize {
                            marker.addNewMarker(markerBase)
                            
                            self.markersArray[index] = marker
                            self.markersArray[indexBase].isFlag = true
                            
                            break search
                        }
                    }
                    
                }
            }
            
            var index = 0
            
            // remove marker intersect
            while index < self.markersArray.count {
                let marker = self.markersArray[index]
                
                if marker.isFlag == true {
                    self.markersArray.removeAtIndex(index)
                } else {
                    index++
                }
            }
        }
        
        
    }
    
    /** Create view with marker and convert to image */
    func getImageFromMarkerData(markerData: MarkerData, result: (image: UIImage) -> Void) -> Void {
        
        // Base view
        let view = UIView(frame: CGRectMake(0, 0, 30, 60))
        
        // Lable
        let label = UILabel(frame: CGRectMake(0, 30, 30, 20))
        
        label.numberOfLines = 0
        label.textAlignment = .Center
        
        
        if markerData.arrayUser.count == 1 {
            view.frame.size.height = 60

            // Icon
            let icon = UIImageView(frame: CGRectMake(0, 0, 30, 30))
            
            label.frame.origin.y = 30
            label.frame.size.height = 30
            
            label.font = UIFont (name: "HelveticaNeue", size: 9)
            
            label.text = markerData.arrayUser.last?.fullName
            
            // request get image
            request(.GET, (markerData.arrayUser.last?.imageAddress)!)
                .responseImage { response in
                    
                    if let image = response.result.value {
                        icon.image = image.af_imageAspectScaledToFillSize(CGSizeMake(30, 30))
                        
                        // add icon and label
                        view.addSubview(icon)
                        view.addSubview(label)
                        
                        // get image
                        result(image: self.getImageFromView(view))
                    } else {
                        result(image: UIImage())
                    }
            }

        } else {
            view.frame.size.height = 30
            
            label.frame.origin.y = 0
            label.frame.size.height = 30
            
            label.font = UIFont (name: "HelveticaNeue", size: 12)
            
            label.text = "\(markerData.arrayUser.count)"
            
            view.backgroundColor = UIColor.whiteColor()
            view.layer.borderColor = UIColor.blackColor().CGColor
            view.layer.borderWidth = 2.0
            view.layer.cornerRadius = view.frame.size.width / 2.0
            
            // add label
            view.addSubview(label)
            
            // get image
            result(image: self.getImageFromView(view))
        }
        
       
    }

    /** Convert UIView to UIImage */
    func getImageFromView(view: UIView) ->  UIImage {
        
        UIGraphicsBeginImageContext(view.bounds.size);
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }

    /** 
        Calculate distance between two coordinates
        Return distance in meters
    */
    func distance(coorA: CLLocationCoordinate2D, coorB: CLLocationCoordinate2D) -> Int {
        
        let locationA : CLLocation = CLLocation(latitude: coorA.latitude, longitude: coorA.longitude)
        let locationB : CLLocation = CLLocation(latitude: coorB.latitude, longitude: coorB.longitude)

        return Int(locationA.distanceFromLocation(locationB))
    }
}



