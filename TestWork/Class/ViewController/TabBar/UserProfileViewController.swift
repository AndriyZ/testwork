//
//  UserProfileViewController.swift
//  TestWork
//
//  Created by ZAV on 09.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit

/** Class UI User profile */
class UserProfileViewController: UIViewController {

    /** Button save all data */
    @IBOutlet weak var saveButton: UIButton!
    
    /** Image user */
    @IBOutlet weak var imageUser: UIImageView!
    
    /** First name user */
    @IBOutlet weak var firstName: UITextField!
    
    /** Last name user */
    @IBOutlet weak var lastName: UITextField!
    
    /** City user */
    @IBOutlet weak var city: UITextField!
    
    /** Country user */
    @IBOutlet weak var country: UITextField!
    
    /** Gender user segment */
    @IBOutlet weak var gender: UISegmentedControl!
    
    /** Picker for send image form iphone */
    var picker:UIImagePickerController = UIImagePickerController()
    
    /** All user data */
    var userData: UserData = UserData()
    
    /** Flag is new image */
    var isNewImage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set delegate and init all obj
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.city.delegate = self
        self.country.delegate = self
        
        self.picker = UIImagePickerController()
        self.picker.delegate = self
        
        self.loadData()
    }
    
    /** Load data form server */
    func loadData() {
        
        Server.getProfile { (status, userData) -> Void in
            if status {
                self.firstName.text = userData.firstName
                self.lastName.text = userData.lastName
                
                self.city.text = userData.city
                self.country.text = userData.country
                
                self.gender.selectedSegmentIndex = userData.typeGender
                
                // reques image from server
                request(.GET, userData.imageAddress)
                    .responseImage { response in
                        
                        if let image = response.result.value {
                            self.imageUser.image = image
                        }
                }
                
                self.userData = userData
            }
        }
        
    }
    
    /** Send all data to server */
    @IBAction func save(sender: AnyObject) {
        
        self.userData.firstName = self.firstName.text!
        self.userData.lastName = self.lastName.text!
        self.userData.city = self.city.text!
        self.userData.country = self.country.text!
        
        self.saveButton.enabled = false
        
        Server.setProfile(self.imageUser.image!, isNewImage: self.isNewImage, userData: self.userData) { (status) -> Void in
            self.saveButton.enabled = true
        }
    }
    
    /** Changed gender */
    @IBAction func changedGender(sender: AnyObject) {
        self.userData.typeGender = self.gender.selectedSegmentIndex
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - Text View Delegate

extension UserProfileViewController: UITextFieldDelegate {
    
    /** Text field return */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension UserProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    /** Edit image user */
    @IBAction func editImage(sender: AnyObject) {
        
        let buttons = ["Take photo", "Choose existing"]
        
        UIActionSheet.showInView(self.view, withTitle: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: buttons) { (let actionSheet:UIActionSheet, let buttonIndex:Int) -> Void in
            
            // select type source
            switch buttonIndex {
            case 0:
                self.picker.sourceType = UIImagePickerControllerSourceType.Camera
            case 1:
                self.picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            default:
                print("\(buttonIndex)")
            }
            
            if buttonIndex >= 0 && buttonIndex <= 1 {
                self.picker.allowsEditing = true
                self.presentViewController(self.picker, animated: true, completion: nil)
            }
        }
    }
    
    /** Get image */
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        self.isNewImage = true
        self.imageUser.image = image
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /** Cancel load image */
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

}