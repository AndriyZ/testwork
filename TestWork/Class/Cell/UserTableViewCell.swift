//
//  UserTableViewCell.swift
//  TestWork
//
//  Created by ZAV on 09.10.15.
//  Copyright © 2015 ZAV. All rights reserved.
//

import UIKit

/** Class UI User table view cell */
class UserTableViewCell: UITableViewCell {

    /** Image user */
    @IBOutlet weak var imageUser: UIImageView!
    
    /** Full name user */
    @IBOutlet weak var nameUser: UILabel!
    
    /** Plcae user (City, Country) */
    @IBOutlet weak var placeUser: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
